import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/clientes")
public class clientesControl {
    @Autowired
    private clientesRepo clientesRepo;

    @GetMapping
    public List<clientesRepo> getAllclientes() {
        return clientesRepo.findAll();
    }

    @PostMapping
    public clientes createclientes(@RequestBody clientes clientes) {
        return clientesRepo.save(clientes);
    }
}
