package com.example.REGISTROCLIENTE;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistroclienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistroclienteApplication.class, args);
	}

}
